#!/bin/bash

# Start Gunicorn processes
echo Starting Gunicorn.
cd /code
python manage.py runserver 0.0.0.0:8000