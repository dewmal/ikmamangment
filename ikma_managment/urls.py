from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexPage.as_view(), name='index'),
    path('news', views.NewsPage.as_view(), name='news'),
    path('add_member', views.add_member, name='add_member'),
]
