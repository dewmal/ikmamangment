# Create your models here.
from datetime import datetime

from django.db import models


class MemberManager(models.Manager):
    def create_(self, first_name, last_name, email, date_of_birth, phone_number, agent_code):
        date_of_birth = datetime.strptime(date_of_birth, '%Y-%m-%d')

        try:
            super_agent = Agent.objects.filter(pk=agent_code).first()
        except:
            pass
        agent = Agent.objects.create(email=email)
        member = self.create(first_name=first_name, last_name=last_name, email=email, date_of_birth=date_of_birth,
                             phone_number=phone_number, super_agent=super_agent,
                             agent_code=agent)
        # do something with the book
        return member


class Agent(models.Model):
    email = models.CharField(max_length=200, default="email")


class Member(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=200)
    date_of_birth = models.DateField()
    phone_number = models.CharField(max_length=20)
    agent_code = models.ForeignKey(Agent, on_delete=models.CASCADE,
                                   null=True,
                                   related_name="agent_code")
    super_agent = models.ForeignKey(Agent, on_delete=models.CASCADE,
                                    null=True,
                                    related_name="super_code")

    objects = MemberManager()
