from django.shortcuts import redirect
# Create your views here.
from django.template.loader import render_to_string
from django.views.generic import TemplateView

from ikma_managment.models import Member


class IndexPage(TemplateView):
    template_name = "register.html"


class ProfilePage(TemplateView):
    template_name = "profile.html"


class NewsPage(TemplateView):
    template_name = "news.html"


def add_member(request):
    if request.POST:
        data = request.POST

        member = Member.objects.create_(
            first_name=data['first_name'],
            last_name=data['last_name'],
            email=data['email'],
            phone_number=data['phone_number'],
            date_of_birth=data['date_of_birth'],
            agent_code=data['agent_code'],
        )

        from django.core.mail import send_mail

        wellcome_message = render_to_string('email.html',
                                            context={'first_name': member.first_name,
                                                     'agent_code': member.agent_code.pk},
                                            request=request)

        send_mail(
            'Ikma.lk Member Registration',
            wellcome_message,
            'info@ikma.lk',
            [member.email],  # , 'isurunathaslic@gmail.com'
            fail_silently=False,
        )
        return redirect('/ikma/news')
    else:
        return redirect('/ikma/')
