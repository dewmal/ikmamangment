# Dockerfile

# FROM directive instructing base image to build upon
FROM python:3.7-slim
# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /code
COPY requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

COPY . ./
RUN ls /code/static


# COPY startup script into known file location in container
COPY start.sh /code/start.sh

# EXPOSE port 8000 to allow communication to/from server
EXPOSE 8000

# CMD specifcies the command to execute to start the server running.
CMD ["/code/start.sh"]
# done!